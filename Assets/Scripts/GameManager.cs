﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager instace;

	public GameObject[] animals;
	public GameObject starsVfx, coin;

	private float time;
	private int coins, currentAnimals, currentStage;
	private bool startGame;
	private int[] totalAnimals = { 6, 9, 12 };
	private Vector2[] positions = { new Vector2(-2, -1), new Vector2(0, 0), new Vector2(2, -1) };

	private HUDController hud;
	private Animator starAnimator;

	void Start () 
	{
		instace = this;

		hud = GetComponent<HUDController> ();
		starAnimator = starsVfx.GetComponent<Animator> ();

		//Init();
		//StartGame (0);
	}

	void Init()
	{
		time = 99;
		coins = 0;
		currentAnimals = 0;
		currentStage = 0;
		startGame = false;
	}

	public void StartGame(int stageAnimals)
	{
		currentStage = stageAnimals;
		startGame = true;

		Init ();
		CreateAnimal ();

		hud.UpdateCoin (coins);
		hud.UpdateTime (time);
		hud.UpdateAnimal (currentAnimals, totalAnimals[currentStage]);
	}

	void Update () 
	{
		if (startGame) 
		{
			time -= Time.deltaTime;
			Mathf.RoundToInt (time);
			hud.UpdateTime (time);
		}

		if (time <= 0) GameOver (false);
	}

	void CreateAnimal()
	{
		int randomAnimal = Random.Range (0, animals.Length);
		int randomPosition = Random.Range (0, positions.Length);
		float offset = animals [randomAnimal].GetComponent<InteractiveObject> ().offset;

		animals [randomAnimal].SetActive (true);
		animals [randomAnimal].transform.localPosition = new Vector2( positions[randomPosition].x, positions[randomPosition].y + offset);
	}

	void CreateCoin()
	{
		int randomPosition = Random.Range (0, positions.Length);
		float offset = coin.GetComponent<InteractiveObject> ().offset;

		coin.SetActive (true);
		coin.transform.localPosition = new Vector2( positions[randomPosition].x, positions[randomPosition].y + offset);
	}

	public void AddCoins(GameObject clickedCoin)
	{
		coins += 100;
		hud.UpdateCoin (coins);
		CreateStar (clickedCoin);
	}

	public void AddAnimal(GameObject clickedAnimal)
	{
		currentAnimals++;

		hud.UpdateAnimal (currentAnimals, totalAnimals[currentStage]);
		CreateStar (clickedAnimal);
		CreateAnimal ();

		if (currentAnimals >= totalAnimals [currentStage]) GameOver (true);
	}

	void CreateStar(GameObject clickedItem)
	{
		clickedItem.SetActive (false);
		starsVfx.SetActive (true);

		starAnimator.Play (0);
		starsVfx.transform.localPosition = new Vector2( clickedItem.transform.position.x + 9.2f, clickedItem.transform.position.y - 0.3f);
	}

	public void GameOver(bool win)
	{
		Init ();
		Start ();
	}

	public void OpenFacebook()
	{
		Application.OpenURL("http://www.google.com/");
	}

	public void QuitGame()
	{
		Application.Quit ();
	}
}
