﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveObject : MonoBehaviour 
{
	public bool animal, coin;
	public float offset;

	void Start () 
	{
		
	}
	
	void Update () 
	{
		
	}

	void OnMouseDown()
	{
		if (animal)
		{
			GameManager.instace.AddAnimal (this.gameObject);
		}
		if (coin)
		{
			GameManager.instace.AddCoins (this.gameObject);
		}
	}
}
