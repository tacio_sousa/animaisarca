﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour 
{
	public Text coinText, timeText, animalsText;

	void Start () 
	{
		
	}

	public void UpdateCoin(int value)
	{
		coinText.text = value.ToString();
	}

	public void UpdateTime(float value)
	{
		timeText.text = value.ToString("N0");
	}

	public void UpdateAnimal(int currentValue, int totalValue)
	{
		animalsText.text = currentValue.ToString() + "/" + totalValue.ToString();
	}
}
