﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour 
{
	public GameObject stageModelBG;

	private GameManager game;

	void Start () 
	{
		game = GetComponent<GameManager> ();
	}

	void Update () 
	{
		
	}

	public void ButtonID(int id)
	{
		stageModelBG.GetComponent<SpriteRenderer>().sprite = Resources.Load <Sprite> ("stages/bgtest" + id.ToString());

		game.StartGame (0);
	}
}
